# The Empower Project Overview

The complexity of data analysis is accelerating exponentially as more data of
different types come available. This poses a significant challenge to
researchers whose domain and training has left them unprepared for the
technical sophistication required to both process and analyze big, complex
datasets. In many areas, such as bioinformatics, analysis workflows include a
mixture of compute intensive steps that often require significant computational
resources to complete, as well as downstream analyses that require highly
interactive visualization and data exploration to interpret. These workflows
also require a wide variety of publicly available tools and methods, making the
computational environment needed to execute all the software in a workflow
complicated and challenging. In addition, researchers also need to be able to
share data and analysis results with collaborators, and are increasingly
required to make their code available and reproducible. Given the technical
breadth of challenges posed by this set of common operations, an end-to-end
software solution that is structured enough to empower researchers, especially
those without significant computational skills, would make great progress in
accelerating science across many domains.

The Empower Project aims at developing a set of extensible, modular tools to address
the entire range of technical challenges facing researchers today. The ideal
end-to-end solution will have the following key properties:

  - enable non-computational *and technically savvy* users to write,
    execute, explore, and share their results and workflows without undue
    burden to their existing analysis lifecycle and practices
  - enable users to explore and access shared datasets while avoiding
    unnecessary data duplication
  - enable technically savvy users to easily develop and share new tools with others
  - encourage good, reproducibile code development practices, including:

    * integrated code tracking
    * appropriate tracking and storage modality for source and transformed data
    * data provenance
    * avoiding unnecessary duplication of data

  - implement flexible enviroment management approaches, including conda,
    singularity, and Docker
  - run on a host of different environments, from individual laptops to
    high performance compute clusters to dedicatd cloud infrastructure
  - be sufficiently modular that institutions can pick,choose, and customize
    different parts of the project to fit their needs

# Current tools

## [gim - git-improved](http://bitbucket.org/bucab/gim.git)

This tool wraps git and enables specifying different storage endpoints for
different types of files. For example, **csv** files may be configured to
update an object with a DOI on zenodo, rather than being tracked in git and
pushed to github.com.  This tool does not modify the behavior of git, it simply
adds additional functionality.

## [meta_worm - META-WORflow Manager](http://bitbucket.org/bucab/meta_worm.git)

An execution engine agnostic to the tools and subworkflow software used in a
workflow.  Runs individual tools and custom code as well as subworkflows
including snakemake, nextflow, or Cromwell (WDL) workflow software.  Manages
the computational environment for different tools with native software, conda
or containerization strategies. Modular and configurable execution endpoint
parameters, e.g. switch execution seamlessly from local to OGE to AWS.

## [empower - end-to-end integration of research workflow tools](http://bitbucket.org/bucab/empower.git)

This tool integrates gim and meta_worm with a shareable sample and file info database,
enabling users to share their tools, workflows, and data together.

## [empower-lab - extension of Jupyter Lab code base that interacts with empower](http://bitbucket.org/bucab/empower-lab.git)

A web-friendly interface into empower and its related tools, with interactive
code editing, generic web app integration (e.g. jbrowse, jupyter notebooks,
RShiny, etc), and sharing.

# The Anatomy of a Data Project

Data analysis projects typically have all of the following types of information:

  1. source data, and possibly associated metadata
  2. transformed data; source data that has been manipulated in some way
  3. code that implements custom analysis to produce that intermediate data
  4. code that connects the input and output of individual pieces of code


## Attributes of source data

The source data is often accessed from a heterogeneous mixture of sources (e.g.
on the local file system, in an S3 bucket, in publicly available databases,
etc). Practically, these data often must all be resident on the same storage
modality to be processed, e.g. downloaded from an S3 bucket to the local file
system. This leads to substantial data duplication, as the size and multitude
of datasets increases. Additionally, many researchers in an institution might be
interested in using the same publicly available source data for different
purposes, leading to yet more data duplication. In principle, this data
duplication can be mitigated through intelligent caching of shared datasets,
but such a solution may require significant system administration overhead and
modification of user behavior. The ideal solution would eliminate as much data
duplication as possible, while being easier for researchers to access than by
the methods they already use.


## Intermediate data

Intermediate data is a byproduct of applying transformations to source data that
enable interpretation. The final outputs of a workflow often contain the most
processed form of data for interpretation, but the intermediate transformed data is
often useful in its own right to other researchers. These analysis outputs are
therefore important products of an analysis, and tracking and sharing all
transformed data is thus desirable. There are growing requirements that
processed data be made available upon publication of a study, but there are no
common conventions for making the data available, often leading to loss of these
intermediate results over time. An ideal solution to this problem would be a
seamless and integrated process built into the project development lifecycle
that would track these files, ensuring full provenance, consistency, and
availability of the results.


## Custom code

The custom code written by a researcher is often the most critical aspect of a
project. In some domains, particularly biology, researchers are increasingly
required to write significant custom code to produce and interpret the their
published results, yet very often do not receive adequate training in coding
practice to ensure accuracy. Custom code also evolves significantly over time,
often leading to unsustainable practices like making multiple copies of a single
script as it changes and disorganized codebases. The popular source control
software git is well suited for avoiding these unideal practices, but
researchers often do not receive training in this tool. Also, the git software
is not designed to store large data files, but rather specializes in tracking
and collaboration of relatively small text files that contain code. It is
therefore an unideal and often problematic solution for tracking source and
intermediate data files. Nonetheless, source and intermediate data files
often coexist in the same place (e.g. a specific directory on the local file
system), making distinguishing these types of files for the purposes of
version control and provenance problematic. An ideal solution would enable
researchers to easily distinguish between source data, transformed data,
and source code, such that each is stored in an appropriate storage modality.


## Glue code

The purpose of the remaining code written by the research is to assemble the
individual tools and custom code together into a sequential workflow that
derives the final analysis objects. There are many existing software packages
that facilitate the efficient and accurate execution of these workflows, but
each one is better suited for different situations, and researchers often have
strong opinions about the tools they use. Thus, the researcher should be able
to use the workflow software that best fits their needs and their specific
computational environment while still enabling the provenance, code tracking,
and sharing operations described above.



